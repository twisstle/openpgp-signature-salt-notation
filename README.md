# OpenPGP Signature Salt Notation

This repository contains a draft proposing to define the "salt"
notation name for OpenPGP version 4 signatures.

The draft can be viewed at https://twisstle.gitlab.io/openpgp-signature-salt-notation/.