---
title: OpenPGP Signature Salt Notation
docname: draft-huigens-openpgp-signature-salt-notation-00
category: std
ipr: trust200902
updates: 4880
area: sec
keyword: Internet-Draft
stand_alone: no
submissionType: IETF
pi:
  toc: yes
  tocdepth: 4
  sortrefs: yes
  symrefs: yes
venue:
  group: "OpenPGP"
  type: "Working Group"
  mail: "openpgp@ietf.org"
  arch: "https://mailarchive.ietf.org/arch/browse/openpgp/"
  repo: "https://gitlab.com/twisstle/openpgp-signature-salt-notation"
  latest: "https://twisstle.gitlab.io/openpgp-signature-salt-notation/"
author:
  -
    ins: D. Huigens
    name: Daniel Huigens
    role: editor
    org: Proton AG
    street: Route de la Galaise 32
    code: 1228
    city: Plan-les-Ouates
    country: Switzerland
    email: d.huigens@protonmail.com
normative:
  crypto-refresh:
    title: OpenPGP
    date: 4 January 2024
    author:
      -
        ins: P. Wouters
      -
        ins: D. Huigens
      -
        ins: J. Winter
      -
        ins: N. Yutaka
    target: https://datatracker.ietf.org/doc/html/draft-ietf-openpgp-crypto-refresh-13
  RFC2119:
  RFC4880:
informative:
  PSSLR17:
    target: https://eprint.iacr.org/2017/1014
    title: Attacking Deterministic Signature Schemes using Fault Attacks
    author:
      -
        ins: D. Poddebniak
      -
        ins: J. Somorovsky
      -
        ins: S. Schinzel
      -
        ins: M. Lochter
      -
        ins: P. Rösler
    date: October 2017
--- abstract

This document defines the "salt" Notation Name for OpenPGP version 4 signatures.
This can be used to salt version 4 signatures in a backwards-compatible way.

--- middle

# Introduction

The crypto refresh [crypto-refresh] of the OpenPGP standard [RFC4880] introduces version 6 signatures, which are salted.
This has several benefits, such as preventing fault attacks against EdDSA signatures.
This document introduces a "salt" Notation Name so that version 4 signatures can benefit from some of the same advantages in a backwards-compatible way.
Note, however, that the notations are not hashed first in the signature, and thus this does not automatically benefit from _all_ benefits described in Section 13.2 of [crypto-refresh].
Therefore, this proposal is not intended to delay or remove the necessity for deploying version 6 keys and signatures.

# Conventions Used in This Document

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as described in [RFC2119].
Any implementation that adheres to the format and methods specified in this document is called a compliant application.
Compliant applications are a subset of the broader set of OpenPGP applications described in [RFC4880] and the OpenPGP crypto refresh [crypto-refresh].
Any [RFC2119] keyword within this document applies to compliant applications only.

# Motivation

Salting signatures can prevent certain attacks, such as fault attacks against EdDSA [PSSLR17].
In version 6 signatures, a salt was added (see Section 5.2.3 and Section 13.2 of [crypto-refresh]).
For version 4 signatures, some implementations (originating with Sequoia-PGP) add a signature notation to add a salt in a backwards-compatible manner.
To comply with [crypto-refresh], implementations have created implementation-specific notation names under a domain they control (such as "salt@notations.sequoia-pgp.org").
However, this adds some overhead for each created signature, compared to a shorter notation name in the IETF namespace.
Additionally, it adds to the distinguishability of OpenPGP artifacts, unless all implementations use the same notation name.
For implementations that wish to disguise which implementation created any given artifact, it may be preferable to use only notations from the IETF namespace.

# Notation Data Subpacket Type {#notation}

This document defines a new Notation Data Subpacket Type for use with OpenPGP, extending Table 7 of [crypto-refresh].

{: title="Signature Salt Notation registration" #new-notation}
Notation Name | Data Type | Allowed Values
--------------|-----------|---------------
 salt         | binary    | random data

This notation can be used to store a random salt in version 4 signatures (see section 5.2 of [crypto-refresh]).
The length of the salt MUST match the "V6 signature salt size" value defined for the hash algorithm as specified in Table 23 of [crypto-refresh].
The "human-readable" flag MUST NOT be set for this notation name.

# Security Considerations

Unlike the salt in version 6 signatures (as defined in Section 5.2.3 of [crypto-refresh]), which is hashed at the start before all other data, a salt in a Notation Data subpacket is hashed after the data to be signed (see Section 5.2.3.6 of [crypto-refresh]).
Because of this, the salt notation may not prevent chosen prefix collision attacks like version 6 signatures do (see Section 13.2 of [crypto-refresh]).
Therefore, this mechanism is not intended to replace or delay the deployment of version 6 signatures.
It should only be used when the use of version 4 signatures is required (e.g. for compatibility reasons).

# IANA Considerations

IANA is requested to add the registration in {{new-notation}} to the "OpenPGP Signature Notation Data Subpacket Types" registry, with a reference to this document in the "Reference" column.

# Acknowledgements

The idea and first implementation of a salt notation came from Sequoia-PGP.
